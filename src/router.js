import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Contact from './views/Contact.vue'
import Explore from './views/Explore.vue'
import Book from './views/Book.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/explore',
      name: 'explore',
      component: Explore
    },
    {
      path: '/book',
      name: 'book',
      component: Book
    },
  ]
})
