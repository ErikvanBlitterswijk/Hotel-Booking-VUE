import Room from './Room.js'

export default class Hotel{
  constructor(){
    this.id= 0;
    this.name= '';
    this.address='';
    this.description='';
    this.rooms=[];
  }

  static fromJSON(json) {
    const self = new Hotel();

    self.id = json.id;
    self.name = json.name;
    self.description = json.description;
    self.address = json.address;

    self.rooms = json.rooms.map(Room.fromJSON);

    return self;
  }
}
