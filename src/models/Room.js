export default class Room{
  constructor(){
    this.price="";
    this.name="";
    this.size="";
    this.bed="";
    this.quantity="";
  }

  static fromJSON(json) {
    const self = new Room();

    self.price = json.price;
    self.name = json.name;
    self.size = json.size;
    self.bed = json.bed;
    self.quantity = json.quantity;

    return self;
  }
}
